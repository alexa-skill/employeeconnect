# EmployeeConnect

##Running the application locally
There are several ways to run a Spring Boot application on your local machine. One way is to execute the main method in the de.codecentric.springbootsample.Application class from your IDE.

Alternatively you can use the Spring Boot Maven plugin like so:

```
mvn spring-boot:run

```

##Clone

```
    git clone https://alexa-skill@bitbucket.org/alexa-skill/employeeconnect.git

```

##Access

```
    http://localhost:8080/
```