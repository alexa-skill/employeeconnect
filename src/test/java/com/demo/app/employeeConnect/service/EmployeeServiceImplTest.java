package com.demo.app.employeeConnect.service;

import com.demo.app.employeeConnect.model.Employee;
import com.demo.app.employeeConnect.repository.EmployeeRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class EmployeeServiceImplTest {

    EmployeeServiceImpl employeeService = new EmployeeServiceImpl();

    @Mock
    private EmployeeRepository employeeRepository;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        employeeService.setEmployeeRepository(employeeRepository);
    }

    @Test
    public void testGetAllEmployee(){
        List<Employee> employeeList = getEmployeeList();

        when(employeeRepository.findAll()).thenReturn(employeeList);

        List<Employee> actualEmployeeList = employeeService.getAllEmployee();

        verify(employeeRepository,times(1)).findAll();

        assertTrue(!actualEmployeeList.isEmpty());

        assertEquals("ABC",actualEmployeeList.get(0).getFirstName());
    }

    @Test
    public void testSaveEmployee(){
        Employee employee = getEmployeeList().get(0);

        when(employeeRepository.save(employee)).thenReturn(employee);

        employeeService.saveEmployee(employee);

        verify(employeeRepository,times(1)).save(any(Employee.class));
    }

    private List<Employee> getEmployeeList(){
        List<Employee> employeeList = new ArrayList<>();

        Employee e1= new Employee();
        e1.setFirstName("TEST1");
        employeeList.add(e1);
        Employee e2= new Employee();
        e2.setFirstName("ABC");
        employeeList.add(e2);
        return employeeList;
    }
    
}
