package com.demo.app.employeeConnect.api;

import com.demo.app.employeeConnect.model.Employee;
import com.demo.app.employeeConnect.service.EmployeeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @PostMapping(value = "/api/employee/create",produces = "application/json")
    @ApiOperation("Create a new Employee")
    public Employee createEmployee(@RequestBody Employee emp) {
        employeeService.saveEmployee(emp);
        return emp;
    }

    @GetMapping(value="/api/employee/findAll",produces = "application/json")
    @ApiOperation("Returns List of all employees")
    public List<Employee> getAllEmployee() {
        return employeeService.getAllEmployee();
    }
}
