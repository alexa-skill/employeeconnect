package com.demo.app.employeeConnect.repository;

import com.demo.app.employeeConnect.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
