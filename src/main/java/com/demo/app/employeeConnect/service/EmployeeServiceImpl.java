package com.demo.app.employeeConnect.service;

import com.demo.app.employeeConnect.model.Employee;
import com.demo.app.employeeConnect.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;


    public void setEmployeeRepository(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public List<Employee> getAllEmployee() {
        List<Employee> allEmployees = employeeRepository.findAll();
        allEmployees = allEmployees.stream().sorted(Comparator.comparing(Employee::getFirstName)).collect(Collectors.toList());
        return allEmployees;
    }

    @Override
    public void saveEmployee(Employee employee) {
         employeeRepository.save(employee);
    }
}
