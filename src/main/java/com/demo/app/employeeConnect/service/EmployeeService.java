package com.demo.app.employeeConnect.service;

import com.demo.app.employeeConnect.model.Employee;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface EmployeeService {

    List<Employee> getAllEmployee();

    void saveEmployee(Employee employee);
}
